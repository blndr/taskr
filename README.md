# Taskr
This is a very simple 'TODO' app !
I uses Flask as a web server and only 1 line of (inline) JavaScript to handle todo lists of users.
You can try it out here : http://taskr-web.herokuapp.com/

## Install requirements
```
. venv/bin/activate
pip install -r requirements.txt
```

## Local execution
```
. venv/bin/activate
./run.sh
```

## Unit test execution
```
. venv/bin/activate
pytest
```