class Task:
    def __init__(self, id: int, name: str):
        self.id = id
        self.name = name
        self.done = False

    def __eq__(self, other):
        return self.id == other.id and self.name == other.name and self.done == other.done

    def __repr__(self):
        return 'Task{id: %d, name: \'%s\', done: %s}' % (self.id, self.name, self.done)
