from app.todo.task import Task


class Work:
    def __init__(self):
        self.tasks = []

    def __contains__(self, item: Task) -> bool:
        return any(task == item for task in self.tasks)

    def __iter__(self):
        for task in self.tasks:
            yield task

    def __getitem__(self, id: int) -> Task:
        for task in self.tasks:
            if task.id == id:
                return task

    def __bool__(self):
        return len(self.tasks) > 0

    def new(self, name: str) -> None:
        self.tasks.append(Task(self._next_id(), name))

    def done(self, id: int) -> None:
        self[id].done = True

    def todo(self):
        return list(filter(lambda task: not task.done, self.tasks))

    def finished(self):
        return list(filter(lambda task: task.done, self.tasks))

    def empty(self):
        self.tasks = []

    def _next_id(self):
        return self.tasks[-1].id + 1 if self else 0
