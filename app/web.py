import os

from flask import Flask
from flask import render_template
from flask import request
from werkzeug.utils import redirect

from app.todo.work import Work

api = Flask(__name__)

users = {
    'abel': Work(),
    'claire': Work()
}


@api.route('/')
def index():
    return redirect('/%s' % list(users.keys())[0])


@api.route('/<user>')
def homepage(user):
    work = users[user]
    return render_template(
        'index.html',
        users=users.keys(),
        user=user,
        todo=work.todo(),
        finished=work.finished()
    )


@api.route('/user/change', methods=['POST'])
def change_work():
    work_name = request.form['user']
    return redirect('/%s' % work_name)


@api.route('/<user>/task', methods=['POST'])
def new(user):
    work = users[user]
    task_name = request.form['name']
    work.new(task_name)
    return redirect('/%s' % user)


@api.route('/<user>/done', methods=['POST'])
def done(user):
    work = users[user]
    id = int(request.form['id'])
    work.done(id)
    return redirect('/%s' % user)


@api.route('/<user>/empty')
def empty(user):
    work = users[user]
    work.empty()
    return redirect('/%s' % user)


if __name__ == '__main__':
    port = int(os.environ.get('PORT', 5000))
    api.run(host='0.0.0.0', port=port)
