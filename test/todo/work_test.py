import unittest

from app.todo.work import Work

from app.todo.task import Task


class WorkTest(unittest.TestCase):
    def setup_method(self, method):
        self.work = Work()

    def test_new_should_add_a_task(self):
        # when
        self.work.new('first task')

        # then
        assert Task(0, 'first task') in self.work

    def test_done_tags_a_task_as_done(self):
        # given
        self.work.new('first task')

        # when
        self.work.done(self.work[0].id)

        # then
        assert self.work[0].done

    def test_tasks_have_auto_incremented_ids(self):
        # given
        self.work.new('first task')
        self.work.new('second task')

        # when
        self.work.new('third task')

        # then
        assert self.work[0].id == 0
        assert self.work[1].id == 1
        assert self.work[2].id == 2

    def test_todo_returns_all_tasks_not_done_yet(self):
        # given
        self.work.new('first task')
        self.work.new('second task')
        self.work.new('third task')

        # when
        self.work.done(self.work[1].id)

        # then
        assert self.work.todo() == [self.work[0], self.work[2]]

    def test_finished_returns_all_tasks_already_done(self):
        # given
        self.work.new('first task')
        self.work.new('second task')
        self.work.new('third task')

        # when
        self.work.done(self.work[1].id)

        # then
        assert self.work.finished() == [self.work[1]]

    def test_empty_should_remove_all_the_tasks(self):
        # given
        self.work.new('first task')
        self.work.new('second task')

        # when
        self.work.empty()

        # then
        assert not self.work
