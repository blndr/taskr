import unittest

from app.todo.work import Work
from app.web import api, users


class ApiTest(unittest.TestCase):
    def setup_method(self, method):
        api.config['TESTING'] = True
        self.api = api.test_client(self)

    def test_index_redirects_to_first_user_page(self):
        # when
        response = self.api.get('/')

        # then
        assert response.status_code == 302
        assert response.location == 'http://localhost/abel'
